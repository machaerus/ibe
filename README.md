# Inference to the Best Explanation

This is an attempt to replicate/extend the series of simulations comparing the performance of Bayesian update vs. IBE (Inference to the best explanation), as presented by Igor Douven in [1].

![screenshot](screenshot.png "Screenshot 2019-11-02")

## Goal

The goal of this project is threefold:

1. To replicate the experiments originally conducted by Igor Douven.

2. To develop a framework that can be used to easily implement, test, and compare different (new) variants of the experiments, including tweaking the parameters that Douven assumed as fixed, but also try other update methods that he did not mention.

3. To build an interactive dashboard that visualizes the experiments, augments intuition, and is fun to use.

## Repository structure

- `simulator.py` - code for the simulation, including abstractions for a biased coin and an epistemic agent, that can all be used semi-independently, and don't rely on the dashboard.

- `app.py`, `layout.py`, `experiments.py` - main code for the interactive dashboard, written with Dash.

- `assets` - static files for the dashboard.

## Usage

You will need Python 3.x. First, install dependencies:

	pip install -r requirements.txt

To use the interactive dashboard, simply run:

	python app.py

and open the URL that you will see in the output (normally 127.0.0.1:8050).

To try the simulations in a terminal, or to develop your own experiments, import the contents of `simulator.py`. The code is quite densly documented. In `experiments.py` you will find an example of how to prepare and run a simulation, and how to analyze the results.

## TODO

Not all the experiments performed by Douven are currently possible to run in this framework. Most importantly, scoring rules and epistemic penalties are still to be implemented.

## Experiments from Douven (2013)

_This section is a work in progress_

### First experiment:

- threshold = .99

- 1000 tosses with each possible bias

- who was first to assert the truth?
- how many more tosses the other player needed to assert the truth?

### Second experiment

- threshold = .99

- random bias

- we play until one player asserts any hypothesis

- if she is correct, she gets one point

- if she is incorrect, the opponent gets on point

- if they both assert the correct h. simultanously, they both receive a point

- if they both assert incorrect hypothesis, nobody gets points

- if one asserts the correct one, and the other an incorrect one, the first gets 2 points

- repeat 100 times to see who gets more points

- repeat the entire game 1000 times


### Third experiment

- 1000 simulations of a sequence of 1000 tosses, for each possible bias

- after each step, measure penalties with different scoring rules (log, Brier)


## References

[1] Douven, I. (2013). Inference to the best explanation, Dutch
books, and inaccuracy minimisation. The Philosophical Quarterly,
63(252), 428-444.
