import dash_core_components as dcc
import dash_bootstrap_components as dbc

layout = [
	dbc.Row(
		className="d-flex align-items-stretch",
		id="main",
		children=[
		dbc.Col(className="sidebar bg-light pt-3", children=[
			dbc.Card([
				dbc.CardHeader("Configuration"),
				dbc.CardBody([
					dbc.Form([
						dbc.InputGroup([
							dbc.InputGroupAddon("Games", addon_type="prepend"),
							dbc.Input(id="games", value="10", className="text-right pr-2")
						], className="mb-2"),
						dbc.InputGroup([
							dbc.InputGroupAddon("Tosses", addon_type="prepend"),
							dbc.Input(id="tosses", value="200", className="text-right pr-2")
						], className="mb-2"),
						dbc.InputGroup([
							dbc.InputGroupAddon("Bias", addon_type="prepend"),
							dbc.Input(id="bias", value="5", className="text-right pr-2"),
							dbc.InputGroupAddon("/ 10", addon_type="append"),
						], className="mb-2"),
						dbc.InputGroup([
							dbc.InputGroupAddon("N", addon_type="prepend"),
							dbc.Input(id="N", value="10", className="text-right pr-2")
						], className="mb-2", style={"display":"none"}),
						dbc.InputGroup([
							dbc.InputGroupAddon("IBE bonus", addon_type="prepend"),
							dbc.Input(id="beta", value="0.1", className="text-right pr-2")
						], className="mb-2"),
						dbc.InputGroup([
							dbc.InputGroupAddon("Certainty threshold", addon_type="prepend"),
							dbc.Input(id="certainty", value="0.99", className="text-right pr-2")
						], className="mb-3"),
						dbc.FormGroup([
							dbc.Checkbox(
								id='freeze_on_certainty',
								checked=False,
								className="mr-2"
							),
							dbc.Label("Freeze beliefs after assertion", html_for="freeze_on_certainty"),
						]),
						dbc.FormGroup(
							[
								dbc.Label("Keep playing until:"),
								dbc.RadioItems(
									options=[
										{
											"label": "Any player asserts any hypothesis",
											"value": "one_assert_any"
										},
										{
											"label": "Any player asserts true hypothesis",
											"value": "one_assert_truth"
										},
										{
											"label": "All players assert any hypothesis",
											"value": "all_assert_any"
										},
										{
											"label": "All players assert true hypothesis",
											"value": "all_assert_truth"
										},
										{
											"label": "Fixed number of tosses",
											"value": "fixed"
										},
									],
									value="fixed",
									id="game_rules",
								),
							]
						)

					]),
					dbc.Button('Run',
							color='primary', id='run-button', size="lg"),

				]),
			], className="mb-3"),
			dbc.Card([
				dbc.CardHeader("Scores"),
				dbc.CardBody(id="score_table", children=[])
			], className="mb-3")
		], width=3),

		dbc.Col([
			dcc.Loading(
				# type="dot",
				children=[
					dcc.Graph(
						id='credences_plot',
						config={'displayModeBar': False},
						style={"height": "calc(0.7 * (100vh - 70px - 40px))"}
					)
				]
			),
			dcc.Loading(
				# type="dot",
				children=[
					dcc.Graph(
						id='frequency_plot',
						config={'displayModeBar': False},
						style={"height": "calc(0.3 * (100vh - 70px - 40px))"}
					)
				]
			)
		], width=9)
	])
]
