#!/usr/bin/env python
import logging
from time import time

import dash
import dash_auth
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
import numpy as np
import pandas as pd

log_format = '[%(asctime)s] [%(levelname)s] %(message)s'
logging.basicConfig(
	level=logging.INFO,
	format=log_format,
	datefmt='%Y-%m-%dT%H:%M:%S%z')

from layout import layout
from experiments import experiment

# ------------------------------------------------------------------------------

app = dash.Dash(__name__,
	external_stylesheets=[
		"https://bootswatch.com/4/sandstone/bootstrap.min.css"
	]
)

# ------------------------------------------------------------------------------

app.layout = html.Div(children=[

	dbc.Navbar([
		dbc.NavbarBrand("Bayes vs Inference to the Best Explanation",
			className="p-3")
	], dark=True, color="primary", id="nav-bar"),

	dbc.Container(fluid=True, children=layout)

])

# ------------------------------------------------------------------------------

@app.callback(
	[
		Output('credences_plot', 'figure'),
		Output('frequency_plot', 'figure'),
		Output('score_table', 'children')
	], [
		Input('run-button', 'n_clicks')
	], [
		State('games', 'value'),
		State('tosses', 'value'),
		State('bias', 'value'),
		State('N', 'value'),
		State('beta', 'value'),
		State('certainty', 'value'),
		State('freeze_on_certainty', 'checked'),
		State('game_rules', 'value'),
	])

def run(
		run_button,
		games,
		tosses,
		bias,
		N,
		beta,
		certainty,
		freeze_on_certainty,
		game_rules,
	):

	start_time = time()

	(figure_credences,
	figure_frequency,
	score_table) = experiment(
		n_games=int(games),
		tosses=int(tosses),
		bias=int(bias),
		N=int(N),
		beta=float(beta),
		certainty=float(certainty),
		freeze_on_certainty=bool(freeze_on_certainty),
		game_rules=str(game_rules),
	)

	elapsed = time() - start_time
	logging.info(f"Elapsed: {elapsed:.2f}")

	return figure_credences, figure_frequency, score_table

# ------------------------------------------------------------------------------

if __name__ == '__main__':
	app.run_server(debug=True, host='0.0.0.0', port=5000)
