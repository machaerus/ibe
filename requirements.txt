dash==1.3.0
dash-bootstrap-components==0.7.1
dash-core-components==1.2.0
dash-html-components==1.0.1
nptyping==0.3.1
numpy==1.17.2
pandas==0.25.1
