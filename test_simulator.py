#!/usr/bin/env python

from .simulator import likelihood

def test_likelihood():

	assert likelihood(1, 5, 10) == .5
	assert likelihood(0, 5, 10) == .5
	assert likelihood(1, 3, 10) == .3
	assert likelihood(0, 3, 10) == .7
	assert likelihood(0, 0, 10) == 1
	assert likelihood(1, 0, 10) == 0
	assert likelihood(0, 10, 10) == 0
	assert likelihood(1, 10, 10) == 1

	assert likelihood(1, 0, 1) == 0
	assert likelihood(1, 1, 1) == 1
	assert likelihood(0, 0, 1) == 1
	assert likelihood(0, 1, 1) == 0

# print("All good babe")
