import logging
from time import time

import numpy as np
from nptyping import Array


TOL = .00001
"""float: Numerical error tolerance.
Used to test the coherence of post-update credences.
"""


def Coin(b: int, N: int=10):
	"""
	Iterator representing a coin with bias = b/N, modeled as a binomial
	stochastic process.

	Parameters
	----------
	b : int
		Nominator of the fraction representing the true bias hypothesis.
		Here necessary to evaluate the freeze condition.

	N : int
		Denominator of the fraction representing the true bias hypothesis.

	Yields
	------
	toss : int
		Result of a single coin toss (1 = heads, 0 = tails).

	"""

	while True:
		toss = np.random.binomial(1, b / N)
		logging.debug(f"Coin tossed: {toss}")
		yield toss


def likelihood(E: int, b: int, N: int = 10):
	"""
	Bernoulli likelihood

	The probability of observing an event E given that the true hypothesis
	(coin bias) is equal to b/N.

	Assuming we consider N+1 hypotheses
	e.g. bias = 0/10, 1/10, 2/10, ..., 10/10
	-> 11 possible hypotheses.

	Parameters
	----------
	E : int
		Result of a single coin toss (1 = heads, 0 = tails).

	b : int
		Nominator of the fraction representing the true bias hypothesis.

	N : int
		Denominator of the fraction representing the true bias hypothesis.

	Returns
	-------
	L : float
		Likelihood of E.
		L = P(E | b, N).

	"""

	L = (b / N) ** E * (1 - b / N) ** (1 - E)

	return L

#-------------------------------------------------------------------------------
# Agent
#-------------------------------------------------------------------------------

class Agent:
	"""
	Epistemic agent, capable of observing series of events (coin tosses),
	and updating her credences with regard to a predefined hypothesis space,
	based on the observed evidence.
	"""

	def __init__(self,
			bias: int,
			N: int,
			method: str="bayes",
			beta: float=.1,
			certainty: float=.99,
			freeze_on_certainty: bool=False,
			game_rules: str="fixed"
		):
		"""
		Parameters
		----------
		bias : int
			Nominator of the fraction representing the true bias hypothesis.
			Here necessary to evaluate the freeze condition.

		N : int
			Denominator of the fraction representing the true bias hypothesis.

		method : str
			Method of belief update. Supported methods are 'bayes' and 'ibe'.

		beta : float
			IBE bonus parameter. See the comments in update().

		certainty : float
			Probability threshold defining the meaning of 'assertion'.

		freeze_on_certainty : bool
			If freeze_on_certainty is True, an agent will stop changing her
			credences once any of them crossed the certainty threshold, or once
			she gains certainty in the TRUE hypothesis (depending on the game
			rules). To freeze on certainty basically means to 'raise your hand'
			as a sign of assertion, and to stop playing further.

		game_rules : str
			One of predefined options, determining the game's stop condition.
			Here necessary to evaluate the freeze condition.

		"""
		# N : number of hypotheses we consider - 1
		self.N = N
		self.hypotheses = np.array([i/self.N for i in range(self.N+1)])
		self.bias = bias,
		# Initialize credences
		self.credence = np.ones(self.N+1) / (self.N+1)
		# self.credence = [1/(self.N+1) for i in range(self.N+1)]
		self.method = method
		self.beta = beta

		self.certainty = certainty
		# sometimes we want an agent to stop changing her mind
		# once she reached certainty about a hypothesis
		self.freeze_on_certainty = freeze_on_certainty
		self.game_rules = game_rules
		self.frozen = False

		# self.observed = np.array([], dtype=np.int16)
		self.observed = []

	def _closest(self, hypotheses: Array[float], freq: float):
		"""
		Find the index of an element of an array of hypotheses that
		is closest to the observed empirical frequency.

		If there are two hypotheses equally close, indices of both will
		be returned.

		Parameters
		----------
		hypotheses : np.ndarray
			Array of floats representing bias hypotheses.

		freq : float
			A number representing the observed frequency.

		Returns
		-------
		argmins : np.ndarray
			Array of indices. Usually containing one element only.

		"""

		dist = np.abs(hypotheses - freq)
		argmins = np.where(dist == np.min(dist))[0]
		return argmins

	def _ibe_bonus(self, i: int, argmins: list):
		"""
		Calculate the IBE bonus value assigned to a hypothesis.

		Parameters
		----------
		i : int
			Hypothesis index.
			Equal to b (bias) parameter of a coin.

		argmins : list
			Indices of all the hypotheses closest to the observed frequency.
			This is a 1- or 2-element list.

		Returns
		-------
		bonus : float
			Bonus value.

		"""

		if i in argmins:
			if len(argmins) == 1:
				bonus = self.beta
			else:
				# we divide the bonus between
				# the two closest hypotheses
				bonus = self.beta / 2
		else:
			bonus = 0.
		return bonus

	def update(self, E: int):
		"""
		Update credences based on observed evidence.

		Depending on the agent's properties, she may, at some point, reach
		the state of certainty, after which she stops changing her credences.
		This is represented by the flag 'frozen'.

		There are two update methods implemented: 'bayes' (standard Bayesian
		conditionalization) and 'ibe' (inference to the best explanation).

		An IBE-updating agent uses an additional parameter beta, representing
		the 'bonus' she assigns to the hypothesis she considers to be the best
		explanation of the observed evidence. In this implementation, following
		Igor Douven [1], I assume that a hypothesis H is the best explanation of
		the observed evidence iff the difference between the bias postulated by
		H and the empirical frequency of all evidence observed so far, is
		smaller than the difference between the empirical frequency and bias
		postulated by any other hypothesis considered. If two different
		hypotheses postulate biases equally distant from the empirical frequency,
		then the bonus is split equally between them.

		References
		----------
			[1] Douven, I. (2013). Inference to the best explanation, Dutch
			books, and inaccuracy minimisation. The Philosophical Quarterly,
			63(252), 428-444.

		Parameters
		----------
		E : int
			Result of a single coin toss (1 = heads, 0 = tails).

		Returns
		-------
		None
		"""

		self.observed.append(E)

		if self.frozen:
			return

		# Bayes
		if self.method == 'bayes':
			denominator = np.sum([
				self.credence[j] * likelihood(E, j)
				for j in range(self.N+1)
			])
			new_credence = np.array([
				self.credence[i] * likelihood(E, i) / denominator
				for i in range(self.N+1)
			])

		# IBE
		elif self.method == 'ibe':
			ratio = np.sum(self.observed) / len(self.observed)
			argmins = self._closest(self.hypotheses, ratio)
			bonus = [self._ibe_bonus(i, argmins) for i in range(self.N+1)]
			denominator = np.sum(
				[self.credence[j] * likelihood(E, j) + bonus[j]
				for j in range(self.N+1)
			])
			new_credence = np.array([
				(self.credence[i] * likelihood(E, i) + bonus[i]) / denominator
				for i in range(self.N+1)
			])

		else:
			raise ValueError("Method not implemented!")

		# coherence check
		assert np.abs(1 - np.sum(new_credence)) < TOL
		self.credence = new_credence

		if self.freeze_on_certainty:
			if self.game_rules in ["one_assert_any", "all_assert_any"]:
				if any(self.credence > self.certainty):
					self.frozen = True
			elif self.game_rules in ["one_assert_truth", "all_assert_truth"]:
				if self.credence[self.bias] > self.certainty:
					self.frozen = True

#-------------------------------------------------------------------------------
# Simulation
#-------------------------------------------------------------------------------

def run_simulation(
		agents: list,
		tosses: int,
		bias: int,
		N: int,
		certainty: float,
		game_rules: str
	):
	"""
	Run a single simulation, which represents a game played by a number of
	epistemic agents. A (possibly) biased coin is tossed until the stop
	condition is met, and after each toss, each player updates their credences
	in each of the predefined hypotheses about the coin bias, according to their
	own update method. The goal of the game is to assert the true hypothesis
	as fast as possible.

	Parameters
	----------
	agents : list
		List of instances of Agent class, representing players in the game.

	tosses : int
		Number of tosses. Only used if game_rules = "fixed".

	bias : int
		Nominator of the fraction representing the true bias hypothesis.

	N : int
		Denominator of the fraction representing the true bias hypothesis.
		Note that there are N+1 possible hypotheses.

	certainty : float
		Probability threshold defining the meaning of "assertion".

	game_rules : str
		One of predefined options, determining the stop condition. See comments
		in the code for the explanation of supported options.

	Returns
	-------
	credences : numpy.ndarray
		A 3D array of shape [tosses, len(agents), N], representing credences
		assigned by each agent to each hypothesis, after each coin toss during
		the simulation.
	"""

	coin = Coin(bias, N)
	credences = []

	i = 1
	while True:
		toss = next(coin)
		for agent in agents:
			agent.update(toss)

		curr_credences = np.array([agent.credence for agent in agents])
		credences.append(curr_credences)

		# Check the stop condition

		if game_rules == "fixed":
			# play fixed number of times
			if i == tosses:
				break

		elif game_rules == "one_assert_any":
			# play until one player is certain
			if any(any(curr_credences[a,:] > certainty)
					for a in range(len(agents))):
				break

		elif game_rules == "all_assert_any":
			# play until all players are certain
			if all(any(curr_credences[a,:] > certainty)
					for a in range(len(agents))):
				break

		elif game_rules == "one_assert_truth":
			# play until one player asserts the truth
			if any(curr_credences[a, bias] > certainty
					for a in range(len(agents))):
				break

		elif game_rules == "all_assert_truth":
			# play until all players assert the truth
			if all(curr_credences[a, bias] > certainty
					for a in range(len(agents))):
				break

		i += 1

	credences = np.array(credences)
	return credences

#-------------------------------------------------------------------------------
# Game scores
#-------------------------------------------------------------------------------

def calculate_scores(game, bias, certainty):
	"""
	Assign score points to each agent participating in a game (single run
	of a simulation) based on whether / how fast they managed to assert
	the true hypothesis.

	Parameters
	----------
	game : numpy.ndarray
		A 3D numpy array of shape [tosses, len(agents), N] representing raw
		results of a simulation, as returned by run_simulation() method.

	bias : int
		Index (nominator) of the true hypothesis.

	certainty : float
		Probability threshold defining the meaning of "assertion".

	Returns
	-------
	scores : tuple
		A tuple (pair) of integers, representing scores for each player.

	"""

	n_players = game.shape[1]

	# credences at the end of the game for each player
	final_credences = np.empty((n_players, game.shape[2]), dtype=np.float16)
	# highest credence for each player
	cmax = np.empty(n_players, dtype=np.float16)
	# hypothesis (index) with highest credence
	hmax = np.empty(n_players, dtype=np.int16)
	# did she make an assertion?
	asserted = np.empty(n_players, dtype=np.bool)
	# did her highest credence correspond with the true hypothesis?
	correct = np.empty(n_players, dtype=np.bool)

	for p in range(n_players):
		final_credences[p] = game[-1, p, :]
		cmax[p] = final_credences[p].max()
		hmax[p] = np.where(final_credences[p] == cmax[p])[0]
		asserted[p] = cmax[p] > certainty
		correct[p] = hmax[p] == bias

	# let's assume for now we have only two players
	# as we want to be able to faithfully replicate Douven's experiment

	# we follow the 2nd experiment

	logging.info(asserted[0])
	logging.info(asserted[1])
	logging.info(correct[0])
	logging.info(correct[1])

	# one asserted and was right
	if asserted[0] and ~asserted[1] and correct[0]:
		scores = (1, 0)
	elif asserted[1] and ~asserted[0] and correct[1]:
		scores = (0, 1)

	# one asserted and was wrong
	elif asserted[0] and ~asserted[1] and ~correct[0]:
		scores = (0, 1)
	elif asserted[1] and ~asserted[0] and ~correct[1]:
		scores = (1, 0)

	# both asserted and both were right
	elif asserted[0] and asserted[1] and correct[0] and correct[1]:
		scores = (1, 1)

	# both asserted and both were wrong
	elif asserted[0] and asserted[1] and ~correct[0] and ~correct[1]:
		scores = (0, 0)

	# both asserted but only one was correct
	elif asserted[0] and asserted[1] and correct[0] and ~correct[1]:
		scores = (2, 0)
	elif asserted[0] and asserted[1] and ~correct[0] and correct[1]:
		scores = (0, 2)

	# this case is only possible if game_rules='fixed', and if no player
	# managed to assert any hypothesis
	else:
		scores = (0, 0)

	return scores
