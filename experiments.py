import logging
from random import randrange

import numpy as np
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
import plotly.graph_objects as go

from simulator import Agent, run_simulation, calculate_scores


def experiment(
		n_games: int,
		tosses: int,
		bias: int,
		N: int,
		beta: float,
		certainty: float,
		freeze_on_certainty: bool,
		game_rules: str,
	):

	"""
	Conduct an experiment, consisting of a number of repeated simulations with
	a predefined group of epistemic agents, analyze the results, and generate
	content for data visualization components, to be used in a Dash app.

	Parameters
	----------
	n_games : int
		Number of simulation repetitions.

	tosses : int
		Number of coin tosses per simulation. Only used if game_rules=='fixed'.

	bias : int
		Nominator of the fraction representing the true bias hypothesis.
		If bias == -1, a random bias is chosen for each simulation.

	N : int
		Denominator of the fraction representing the true bias hypothesis.
		Note that there are N+1 possible hypotheses.

	beta : float
		IBE bonus parameter. See the comments in update().

	certainty : float
		Probability threshold defining the meaning of "assertion".

	freeze_on_certainty : bool
		If freeze_on_certainty is True, an agent will stop changing her
		credences once any of them crossed the certainty threshold, or once
		she gains certainty in the TRUE hypothesis (depending on the game
		rules).

	game_rules : str
		One of predefined options, determining the game's stop condition.
		Supported values are: 'one_assert_any', 'all_assert_any',
		'one_assert_truth', 'all_assert_truth', 'fixed'.
		See simulator.Agent.update for a description of those options.

	Returns
	-------
	figure_credences : dict
		Figure containing a plot of credences assigned by each agent to the true
		hypothesis, for each simulation performed, as a function of the number
		of coin tosses.
		A valid value for dcc.Graph figure parameter.

	figure_frequency : dict
		Figure containing a plot of empirical frequencies of 'head' for each
		simulation, as a function of the number of coin tosses.
		A valid value for dcc.Graph figure parameter.

	score_table : dash_bootstrap_components.Table
		An instance of dbc.Table, containing the total number of points scored
		by each agent during the experiment.

	"""

	figure_credences = {
		'data': [],
		'layout': go.Layout(
			margin={
				"l": 50,
				"r": 15,
				"t": 15,
				"b": 20
			},
			legend=dict(x=.01, y=1.04, orientation="h"),
			yaxis=dict(range=[-0.03,1.03]),
		)
	}

	figure_frequency = {
		'data': [],
		'layout': go.Layout(
			margin={
				"l": 50,
				"r": 15,
				"t": 15,
				"b": 20
			},
			legend=dict(x=.01, y=1.1, orientation="h"),
			yaxis=dict(range=[-0.1,1.1]),
		)
	}

	results = []
	biases = []

	for i in range(n_games):

		if bias == -1:
			random_bias = randrange(0, N)
		else:
			random_bias = bias

		# define the agents
		agents = [
			Agent(
				random_bias, N, 'bayes',
				freeze_on_certainty=freeze_on_certainty,
				game_rules=game_rules
			),
			Agent(
				random_bias, N, 'ibe', beta=beta,
				freeze_on_certainty=freeze_on_certainty,
				game_rules=game_rules
			)
		]

		# run the simulation
		credences = run_simulation(
			agents=agents,
			tosses=tosses,
			bias=random_bias,
			N=N,
			certainty=certainty,
			game_rules=game_rules,
		)

		results.append(credences)
		biases.append(random_bias)

		# credences in the true hypothesis
		true_credences = credences[:, :, random_bias]

		# pre-compute it for convenience, will be used as a plot index
		x = list(range(len(true_credences)))

		observed = np.array(agents[0].observed)
		observed_ratios = list(map(lambda j: sum(observed[:j+1]) / (j+1), x))

		showlegend = True if i == 0 else False

		# calculate plot opacity
		opacity = max(.3, 1 / n_games)

		# Despite the slightly confusing naming in Plotly,
		# Scatter is actually a line plot here.

		#-----------------------------------------------------------------------
		# Credences plot

		figure_credences['data'].append(
			go.Scatter(
				x=x,
				y=true_credences[:,0], mode='lines',
				line=dict(color=f'rgba(27, 20, 100, {opacity})', width=2),
				hoverinfo='skip',
				legendgroup="Bayes",
				name="Bayes",
				showlegend=showlegend,
			)
		)

		figure_credences['data'].append(
			go.Scatter(
				x=x,
				y=true_credences[:,1],
				mode='lines',
				line=dict(color=f'rgba(238, 90, 36, {opacity})', width=2),
				hoverinfo='skip',
				legendgroup="IBE",
				name="IBE",
				showlegend=showlegend,
			)
		)

		#-----------------------------------------------------------------------
		# Empirical frequencies plot

		figure_frequency['data'].append(
			go.Scatter(
				x=x,
				y=observed_ratios,
				mode='lines',
				line=dict(color=f'rgba(181, 52, 113, {opacity})', width=2),
				hoverinfo='skip',
				name="Observed frequency",
				showlegend=showlegend,
			)
		)

	#---------------------------------------------------------------------------

	# calculate the points scored by each agent in each game (simulation).
	scores = np.array([
		calculate_scores(game_results, b, certainty)
		for game_results, b in zip(results, biases)
	])

	logging.info(scores)
	logging.info(scores.shape)

	score_table = dbc.Table([
		html.Thead(html.Tr([
			html.Th("Player"),
			html.Th("Points"),
			# html.Th("%")
		])),
		html.Tbody([
			html.Tr([
				html.Td("Bayes"),
				html.Td(np.sum(scores[:,0])),
				# html.Td(f"{100 * win_counts[0] / np.sum(win_counts):.2f}")
			]),
			html.Tr([
				html.Td("IBE"),
				html.Td(np.sum(scores[:,1])),
				# html.Td(f"{100 * win_counts[1] / np.sum(win_counts):.2f}")
			]),
		])
	])

	# persist for easier analysis
	logging.info("Persisting results")
	np.save("data/experiment.npy", np.array(results))

	return figure_credences, figure_frequency, score_table
